﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <body>
    <h2>'Μέγα Λεξικό της ρωμαϊκής και βυζαντινής περιόδου" (από 146 πχ έως 1100 μ.χ.)</h2><br/>
    <h4>Λέξεις σελ. 567</h4><br/>
    <table border="1">
      <tr bgcolor="#ffac8f">
        <th>Λέξη</th>
        <th>Γραμμ.</th>
        <th>Ετυμολογία</th>
        <th>Ορισμός</th>
        <th>Αναφορά</th>
        
      </tr>
      <xsl:for-each select="tei/text/body/entry">
      <tr>
        <td><xsl:value-of select="case" /></td>
        <td><xsl:value-of select="gram" /></td>
        <td><xsl:value-of select="etym" /></td>
        <td><xsl:value-of select="def" /></td>
        <td><xsl:value-of select="oRef" /></td>
      </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>

